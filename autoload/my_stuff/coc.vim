function! my_stuff#coc#install_extensions() abort
  execute ":CocInstall coc-json"
  execute ":CocInstall coc-yaml"
  execute ":CocInstall coc-vimlsp"
  execute ":CocInstall coc-rls"
  execute ":CocInstall coc-docker"
  execute ":CocInstall coc-sh"
  execute ":CocInstall coc-emoji"
  "execute ":CocInstall coc-solargraph"
endfunction
