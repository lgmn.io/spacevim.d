function! my_stuff#vimwiki#config() abort
  let g:vimwiki_list = [{ 
        \ 'auto_diary_index': 1,
        \ 'auto_export': 1,
        \ 'auto_tags': 1,
        \ 'auto_toc': 1,
        \ 'diary_header': 'DevLog',
        \ 'diary_index': 'devlog',
        \ 'diary_rel_path': 'devlog',
        \ 'maxhi': 1,
        \ 'path': '~/Notes', 
        \ 'path_html': '~/Notes/.html', 
        \ 'template_default': 'default',
        \ 'template_ext': '.html',
        \ 'template_path': '~/Notes/.templates/',
        \  }]
  let g:vimwiki_hl_headers = 1
  let g:vimwiki_global_ext = 1
  let g:vimwiki_use_mouse = 1
  let g:vimwiki_folding = 'syntax'
  " let g:vimwiki_listsyms = '✗○◐●✓'
  " let g:vimwiki_listsym_rejected = '✗'
  let g:vimwiki_map_prefix = '<Leader>k'
  call vimwiki#vars#init()
endfunction
