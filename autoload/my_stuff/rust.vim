function! my_stuff#rust#config() abort
  let g:rust_fold = 0
  let g:rustfmt_autosave = 1
  let g:rust_recommended_style = 0
  let g:rust_clip_command = 'xsel -ib'
endfunction
