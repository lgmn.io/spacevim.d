scriptencoding utf-8

let s:SYS = SpaceVim#api#import('system')
let s:JOB = SpaceVim#api#import('job')
let s:FILE = SpaceVim#api#import('file')
let s:HI = SpaceVim#api#import('vim#highlight')

function! my_stuff#spacevim#config_dir() abort
  return expand('~/.SpaceVim.d')
endfunction

function! my_stuff#spacevim#repo_dir() abort
  return expand('~/.SpaceVim')
endfunction

function! my_stuff#spacevim#custom_keybinds() abort
  call SpaceVim#custom#SPC('nnoremap', ['g', 'a'], 'Gina add %', 'stage current file', 1)
  call SpaceVim#custom#SPC('nore', ['b', 'F'], 'DefxBuf', 'Open File Manager (Buffer)', 1)
endfunction

function! my_stuff#spacevim#custom_commands() abort
  command! -bang Q quit
  command! -bang W write
  command! -nargs=+ SaveUrl call saveurl#save(<f-args>)
  command! -nargs=1 -complete=custom,findbin#complete Findbin call findbin#find(<q-args>)
  command! CursorHighlight  echo s:HI.syntax_at()
  command! SPEditAutoload   tabedit ~/.SpaceVim.d/autoload/my_stuff/spacevim.vim
endfunction

function! my_stuff#spacevim#gnvim_settings() abort
  if has_key(g:, 'gnvim') " enabled when in gNVIM.
    " Add cursor blinking.
    " set guicursor=n-v-c:block-blinkon600,i-ci-ve:ver25-blinkon400,r-cr:hor20,o:hor50
  endif
endfunction

function! my_stuff#spacevim#vim_settings() abort
  " Allow us to run vader tests on SpaceVim.
  " set rtp+=~/SpaceVim/SpaceVim/build/vader
  set autoread
  set history=2000

  " Fixes syntax highlighting for JSONC (like vscode).
  autocmd FileType json syntax match Comment +\/\/.\+$+
endfunction

function! my_stuff#spacevim#after_hook() abort
  call my_stuff#spacevim#custom_commands()
  call my_stuff#spacevim#custom_keybinds()
  call my_stuff#spacevim#gnvim_settings()
  call my_stuff#spacevim#vim_settings()
  call my_stuff#rust#config()
  call my_stuff#vimwiki#config()

  " Vim development
  " lint:  vint
  " install: pip install --pre vim-vint
  let g:neomake_vim_vint_maker = {
        \ 'exe': 'vint',
        \ 'args': ['--style-problem', '--no-color',
        \          '-f', '{file_path}:{line_number}:{column_number}:{severity}:{description} ({policy_name})'],
        \ 'errorformat': '%f:%l:%c: %m',
        \ 'output_stream': 'stdout',
        \ }
  let g:neoformat_enabled_html  = 0
  let g:LanguageClient_loggingFile = my_stuff#spacevim#config_dir() + '/language-client.log'
  let g:neomru#file_mru_ignore_pattern = '^[a-z]\+://'
  let g:leaderGuide_flatten         = 1
  let g:indentLine_char         = ' '
  let g:indentLine_setColors    = 0
  let g:indentLine_conceallevel = 0
  let g:indentLine_fileTypeExclude = [ 'vim']
endfunction

function! my_stuff#spacevim#before_hook() abort
  return
endfunction
