"=============================================================================
" terraform.vim --- SpaceVim Terraform layer
" Author: Jake Logemann < jake.logemann at gmail.com >
" URL: https://spacevim.org
" License: NO ASSERTION
"=============================================================================

function! SpaceVim#layers#lang#terraform#plugins() abort
  return [
  \ ['hashivim/vim-hashicorp-tools',{ 'merged' : 0, 'loadconf' : 1}],
  \ ]
endfunction

function! SpaceVim#layers#lang#terraform#config() abort
  augroup spacevim_layer_terraform
    autocmd!
  augroup END
endfunction

" vim:set et sw=2 cc=80:
