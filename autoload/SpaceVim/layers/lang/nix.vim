"=============================================================================
" nix.vim --- SpaceVim Nix/NixOS layer
" Author: Jake Logemann < jake.logemann at gmail.com >
" URL: https://spacevim.org
" License: NO ASSERTION
"=============================================================================

function! SpaceVim#layers#lang#nix#plugins() abort
  return [
  \ ['LnL7/vim-nix',{ 'merged' : 0, 'loadconf' : 1}],
  \ ]
endfunction

function! SpaceVim#layers#lang#nix#config() abort
  call SpaceVim#mapping#space#regesit_lang_mappings('nix', function('s:language_specified_mappings'))
  augroup spacevim_layer_lang_nix
    autocmd!
    autocmd FileType nix call s:set_locals()
    
    autocmd FileType nix silent! ':IndentLinesEnable'
  augroup END
endfunction

" s:set_locals() - 
"   Sets all buffer-local variables for the current buffer (assumes the buffer
"   filetype is correct).
function! s:set_locals() abort
  " For some reason these settings fix indentation in Nix files with auto
  " formatting.
  setlocal nosmartindent indentkeys-=#

  setlocal list listchars=trail:•,tab:→→

  " Removes tabs entirely, fuck tabs. Just No.
  setlocal nosmarttab expandtab tabstop=2 shiftwidth=2

  " Seems reasonable, although this may not be necessary with the plugin...
  setlocal nofoldenable foldmethod=indent foldlevelstart=1 

  retab
  silent! ':StripWhitespace' 
endfunction

" s:language_specified_mappings() - 
"   Sets all filetype-specific keybinds.
function! s:language_specified_mappings() abort
  call SpaceVim#mapping#space#langSPC('nnoremap', ['l', 'r'],
        \ 'tabnew', 'TabNew', 1)

endfunction


" vim:set et sw=2 cc=80:
