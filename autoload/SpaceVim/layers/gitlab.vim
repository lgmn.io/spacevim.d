function! SpaceVim#layers#gitlab#config() abort
  call SpaceVim#layers#gitlab#set_globals()
  " call SpaceVim#mapping#space#regesit_lang_mappings('rust', function('SpaceVim#layers#gitlab#set_lang_maps'))
  augroup gitlab_layer_ftdetect
	  au! BufRead,BufNewFile ISSUE_EDITMSG call SpaceVim#layers#gitlab#set_buf_filetype('issue')
	augroup END
endfunction

function! SpaceVim#layers#gitlab#set_buf_filetype(kind) abort
  """ Applied when FileType detected.
  call SpaceVim#layers#gitlab#set_globals()
	let b:neomake_open_list = 0

  if a:kind == 'issue'
    setfiletype markdown
    normal 4dd
    " Disable folding (for now?).
    setlocal nofoldenable
    setlocal wrap linebreak nolist
    " Enable spell check.
    setlocal spell
  endif
endfunction

function! SpaceVim#layers#gitlab#set_globals() abort
  " let g:rustfmt_fail_silently = 0
endfunction

function! SpaceVim#layers#gitlab#set_lang_maps() abort
  function! LSMap(type, keys, cmd, desc) abort
    let l:keys = ['l'] + a:keys
    call SpaceVim#mapping#space#langSPC(a:type, l:keys, a:cmd, a:desc, 1)
  endfunction

  " TODO: Should only enter text, not execute for commands with params.
  call LSMap('nmap', ['m', 'l'], '!lab mr list', 'lab:merge:ls')
  call LSMap('nmap', ['m', 'A'], '!lab mr approve', 'lab:merge:approve')
  call LSMap('nmap', ['m', 'M'], '!lab mr merge', 'lab:merge:merge')

endfunction

function! SpaceVim#layers#gitlab#plugins() abort
    return [ ]
endfunction

