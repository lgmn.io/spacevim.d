function! SpaceVim#layers#rust_tweaks#config() abort
  call SpaceVim#layers#rust_tweaks#set_globals()
  call SpaceVim#mapping#space#regesit_lang_mappings('rust', function('SpaceVim#layers#rust_tweaks#set_lang_maps'))
  autocmd FileType rust call SpaceVim#layers#rust_tweaks#set_buf_filetype()
endfunction

function! SpaceVim#layers#rust_tweaks#set_buf_filetype() abort
  """ Applied when FileType detected.
  call SpaceVim#layers#rust_tweaks#set_globals()
	let b:neomake_open_list = 0
  let b:rustfmt_autosave_if_config_present = 1
  setlocal foldenable
  setlocal nospell
endfunction

function! SpaceVim#layers#rust_tweaks#set_globals() abort
	let g:neomake_open_list = 0
  let g:rust_cargo_check_examples = 1
  let g:rust_cargo_check_all_features = 1
  let g:rust_cargo_check_tests = 1
  let g:rust_cargo_check_benches = 1
	let g:rust_conceal = 1
	let g:rust_conceal_mod_path = 0
	let g:rust_fold = 2
	let g:rustfmt_autosave_if_config_present = 0
	let g:rustfmt_emit_files = 1
  let g:neomake_echo_current_error = 1
  let g:neomake_virtualtext_current_error = 0
  let g:neomake_virtualtext_prefix =  "❯ "
  let g:rust_keep_autopairs_default = 1
  let g:rust_recommended_style = 0
  let g:rust_use_custom_ctags_defs = 1
  let g:rustfmt_autosave = 1
  let g:rustfmt_fail_silently = 0
endfunction

function! SpaceVim#layers#rust_tweaks#set_lang_maps() abort
  function! LSMap(type, keys, cmd, desc) abort
    let l:keys = ['l'] + a:keys
    call SpaceVim#mapping#space#langSPC(a:type, l:keys, a:cmd, a:desc, 1)
  endfunction

  call LSMap('nmap', ['l'], 'Cargo clippy', 'cargo:clippy')
  call LSMap('nmap', ['F'], 'Cargo fmt',    'cargo:fmt')
  call LSMap('nmap', ['B'], 'Cargo build --release',  'cargo:build:release')
  call LSMap('nmap', ['b'], 'Cargo build',  'cargo:build')
  call LSMap('nmap', ['c'], 'Cargo check',  'cargo:check')
  call LSMap('nmap', ['d'], 'Cargo doc',    'cargo:doc')
  call LSMap('nmap', ['x'], 'Cargo clean',  'cargo:clean')

  if SpaceVim#layers#lsp#check_filetype('rust')
    nnoremap <silent><buffer> K :call SpaceVim#lsp#show_doc()<CR>
    call LSMap('nmap', ['D'], 'call SpaceVim#lsp#show_doc()',  'show docs')
    call LSMap('nmap', ['e'], 'call SpaceVim#lsp#rename()',  'rename symbol')
    call LSMap('nmap', ['r'], 'call SpaceVim#lsp#references()',  'references')
  endif
endfunction

function! SpaceVim#layers#rust_tweaks#plugins() abort
    return [ ]
endfunction

