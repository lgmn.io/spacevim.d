EDITOR          := "nvim"
spacevim_repo   := env_var("HOME") + "/.SpaceVim"
spacevim_config := env_var("HOME") + "/.SpaceVim.d"
coc_extensions  := "coc-gitignore coc-svg coc-snippets coc-rust-analyzer coc-yaml coc-vimlsp coc-lists coc-git coc-highlight coc-python coc-json coc-html coc-rls coc-sh"

repo_git        := "git --git-dir="+spacevim_repo+"/.git --work-tree="+spacevim_repo
config_git      := "git --git-dir="+spacevim_config+"/.git --work-tree="+spacevim_config

edit:
  {{ EDITOR }} -np \
    ./Justfile ./init.toml \
    ./autoload/my_stuff/spacevim.vim \

update: update-repo update-spacevim

update-repo:
  {{ config_git }} pull --rebase --tags --prune --autostash --ff-only

update-spacevim:
  {{ repo_git }} pull --rebase --tags --prune --autostash --ff-only

commit-changes:
  git add -p
  git commit 
  git push

install-coc-exts:
  {{ EDITOR }} -c 'CocInstall -sync {{coc_extensions}}|q'
